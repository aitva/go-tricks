module gitlab.com/aitva/go-tricks

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/stretchr/testify v1.2.2 // indirect
	github.com/stripe/stripe-go v0.0.0-20181201002941-0bf0051f725a
	github.com/urfave/cli v1.20.0
	golang.org/x/net v0.0.0-20181201002055-351d144fa1fc // indirect
	golang.org/x/text v0.3.0 // indirect
)
