package main

import (
	"fmt"
	"os"

	stripe "github.com/stripe/stripe-go"
	"github.com/stripe/stripe-go/coupon"
	"github.com/stripe/stripe-go/customer"
	"github.com/stripe/stripe-go/invoice"
	"github.com/stripe/stripe-go/sub"
	"github.com/urfave/cli"
)

var ErrNotFound = fmt.Errorf("not found")

func main() {
	app := cli.NewApp()
	app.Name = "invoice"
	app.Usage = "Get an invoice from Stripe before subscription."
	app.Version = "v0.0.0"
	app.Action = mainAction
	app.Flags = []cli.Flag{
		cli.StringFlag{Name: "key", Usage: "Stripe secret API key."},
		cli.StringFlag{Name: "email", Usage: "Customer email."},
		cli.StringFlag{Name: "coupon", Usage: "Coupon for a discount."},
		cli.StringFlag{Name: "plan", Usage: "Plan for the subscription."},
	}

	err := app.Run(os.Args)
	if err != nil {
		_, _ = fmt.Fprintln(os.Stderr, err)
	}
}

func mainAction(c *cli.Context) error {
	key := c.String("key")
	if key == "" {
		return fmt.Errorf("--key flag is required")
	}
	email := c.String("email")
	if email == "" {
		return fmt.Errorf("--email flag is required")
	}

	stripe.LogLevel = 0
	stripe.Key = key

	cus, err := newCustomer(email)
	if err != nil {
		return err
	}

	var coupon *stripe.Coupon
	if secret := c.String("coupon"); secret != "" {
		coupon, err = findCoupon(secret)
		if err != nil {
			return fmt.Errorf("fail to find coupon: %v", err)
		}
	}

	fmt.Printf("got customer with:\n")
	fmt.Printf("    - ID: %s\n", cus.ID)
	fmt.Printf("    - email: %s\n", cus.Email)
	if coupon != nil {
		fmt.Printf("    - coupon: %s\n", coupon.Name)
	}
	fmt.Printf("\n")

	planID := c.String("plan")
	if planID == "" {
		return nil
	}
	sub, err := newSubscription(cus.ID, coupon.ID, planID)
	if err != nil {
		return fmt.Errorf("fail to create subscription: %v", err)
	}
	fmt.Printf("got subscription with:\n")
	fmt.Printf("    - ID: %s\n", sub.ID)
	fmt.Printf("\n")

	in, err := findUpcomingInvoice(cus.ID)
	if err != nil {
		return fmt.Errorf("fail to find invoice: %v", err)
	}
	fmt.Printf("got invoice with:\n")
	fmt.Printf("    - ID: %s\n", in.ID)
	fmt.Printf("    - subtotal: %d\n", in.Subtotal)
	fmt.Printf("    - total: %d\n", in.Total)
	fmt.Printf("\n")

	return nil
}

// newCustomer returns a customer with the given email from Stripe.
func newCustomer(email string) (*stripe.Customer, error) {
	c, err := findCustomer(email)
	if err == nil {
		return c, nil
	} else if err != nil && err != ErrNotFound {
		return nil, fmt.Errorf("fail to find customer: %v", err)
	}

	c, err = createCustomer(email)
	if err != nil {
		return nil, fmt.Errorf("fail to create customer: %v", err)
	}
	return c, nil
}

// createCustomer creates a new customer with the given email.
func createCustomer(email string) (*stripe.Customer, error) {
	params := &stripe.CustomerParams{
		Description: stripe.String("Customer created from gitlab.com/aitva/go-tricks."),
		Email:       stripe.String(email),
	}
	c, err := customer.New(params)
	if err != nil {
		return nil, err
	}
	return c, nil
}

// findCustomer finds an existing customer using an email.
func findCustomer(email string) (*stripe.Customer, error) {
	params := &stripe.CustomerListParams{}
	i := customer.List(params)
	for i.Next() {
		c := i.Customer()
		if c.Email == email {
			return c, nil
		}
	}
	if err := i.Err(); err != nil {
		return nil, err
	}
	return nil, ErrNotFound
}

// findCoupon finds a coupon in Stripe server.
func findCoupon(secret string) (*stripe.Coupon, error) {
	params := &stripe.CouponListParams{}
	i := coupon.List(params)
	for i.Next() {
		c := i.Coupon()
		s, ok := c.Metadata["secret"]
		if !ok {
			continue
		}
		if s == secret {
			return c, nil
		}
	}
	if err := i.Err(); err != nil {
		return nil, err
	}
	return nil, ErrNotFound
}

// newInvoice creates a new invoice using a customer, a coupon and a plan.
// This does not work... Stripe returns an error.
func newInvoice(customer, coupon, plan string) (*stripe.Invoice, error) {
	params := &stripe.InvoiceParams{
		Customer:             stripe.String(customer),
		SubscriptionPlan:     stripe.String(plan),
		SubscriptionQuantity: stripe.Int64(1),
	}
	if coupon != "" {
		params.Coupon = stripe.String(coupon)
	}
	in, err := invoice.New(params)
	if err != nil {
		return nil, err
	}
	return in, nil
}

// findSubscription finds an existing subscription using a customer ID.
func findSubscription(customerID string) (*stripe.Subscription, error) {
	params := &stripe.SubscriptionListParams{
		Customer: customerID,
	}
	i := sub.List(params)
	for i.Next() {
		s := i.Subscription()
		if s.Customer.ID == customerID {
			return s, nil
		}
	}
	if err := i.Err(); err != nil {
		return nil, err
	}
	return nil, ErrNotFound
}

// createSubscription creates a new subscription using a customerID, a couponID and a planID.
func createSubscription(customerID, couponID, planID string) (*stripe.Subscription, error) {
	params := &stripe.SubscriptionParams{
		Customer: stripe.String(customerID),
		Plan:     stripe.String(planID),
		Quantity: stripe.Int64(2),
	}
	if couponID != "" {
		params.Coupon = stripe.String(couponID)
	}
	in, err := sub.New(params)
	if err != nil {
		return nil, err
	}
	return in, nil
}

// newSubscription returns a subscription for the given customer.
func newSubscription(customerID, couponID, planID string) (*stripe.Subscription, error) {
	s, err := findSubscription(customerID)
	if err == nil {
		return s, nil
	} else if err != nil && err != ErrNotFound {
		return nil, fmt.Errorf("fail to find subscription: %v", err)
	}

	s, err = createSubscription(customerID, couponID, planID)
	if err != nil {
		return nil, fmt.Errorf("fail to create subscription: %v", err)
	}
	return s, nil
}

// findUpcomingInvoice finds next invoice for a customer.
func findUpcomingInvoice(customerID string) (*stripe.Invoice, error) {
	params := &stripe.InvoiceParams{
		Customer: stripe.String(customerID),
	}
	i, err := invoice.GetNext(params)
	if err != nil {
		return nil, err
	}
	return i, nil
}
